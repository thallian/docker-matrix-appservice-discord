FROM alpine:3.7 AS builder

RUN apk add --no-cache \
    python \
    nodejs \
    make \
    g++ \
    sqlite-dev

RUN npm install --build-from-source sqlite3

FROM registry.gitlab.com/thallian/docker-confd-env:master

ENV VERSION v0.2.0-rc3

RUN addgroup -g 2222 matrix-bridge
RUN adduser -h /var/lib/matrix-bridge -u 2222 -D -G matrix-bridge matrix-bridge

COPY --from=builder /node_modules/sqlite3 /var/lib/matrix-bridge/node_modules/sqlite3

RUN wget -qO- https://github.com/Half-Shot/matrix-appservice-discord/archive/$VERSION.tar.gz | tar xz -C /var/lib/matrix-bridge --strip 1

RUN apk add --no-cache \
    nodejs \
    sqlite-libs

WORKDIR /var/lib/matrix-bridge
RUN npm install
RUN npm run-script build

RUN mkdir /var/lib/matrix-bridge/db

ADD /rootfs /

RUN chown -R matrix-bridge:matrix-bridge /var/lib/matrix-bridge
ENV HOME /var/lib/matrix-bridge

EXPOSE 9005
