[discord bridge](https://github.com/Half-Shot/matrix-appservice-discord) homeserver for the [matrix](http://matrix.org/) network.

# Volumes
- `/var/lib/matrix-bridge/db`

# Environment Variables
## SERVER_DOMAIN
Domain of the synapse server.

## SERVER_URL
Full url to the synapse server.

## CLIENT_ID
Discord app client id.

## SECRET
Discord app secret.

## BOT_TOKEN
Discord bot token.

# Ports
- 9005
